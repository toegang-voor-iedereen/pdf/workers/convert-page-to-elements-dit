# syntax=docker/dockerfile:1

FROM python:3.11-slim

RUN apt-get update && apt-get -y install gcc g++ tesseract-ocr libtesseract-dev libleptonica-dev pkg-config
RUN apt-get install tesseract-ocr-nld tesseract-ocr-deu tesseract-ocr-fra 

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

CMD ["python3", "-u", "main.py"]