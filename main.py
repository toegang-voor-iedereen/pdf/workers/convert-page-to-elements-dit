import io
import sys, os
import kimiworker
import tesserocr
from PIL import Image, ImageDraw, ImageFont

numberOfConcurrentJobs = int(os.getenv('CONCURRENT_JOBS', 1))
outputBucketName = "elements"
outputFileFormat = "jpg"
outputContentType = 'image/jpeg'
font = ImageFont.load_default()

def jobHandler(log, job, jobId, localFilePath, minio, publish):
    recordId = job.get("recordId")
    outputBucketPath = f"{recordId}/jobs/{jobId}"
    found = minio.bucket_exists(outputBucketName)
    if not found:
        minio.make_bucket(outputBucketName)

    log.info("Opening image")
    image = Image.open(localFilePath)

    api = tesserocr.PyTessBaseAPI(lang='nld')
    api.SetImage(image)

    log.info("Getting elements from image")
    boxes = api.GetComponentImages(tesserocr.RIL.BLOCK, True, True)
    elementCount = len(boxes)
    elements = []
    log.info('Found {} elements in image.'.format(len(boxes)))
    imageWithBoundingBoxes = ImageDraw.Draw(image)
    for component in boxes:
        component_image, xywh, index, _ = component

        imageWithBoundingBoxes.rectangle(((xywh['x'], xywh['y']),(xywh['x']+xywh['w'], xywh['y']+xywh['h'])), outline='Red', width=2)
        imageWithBoundingBoxes.text((xywh['x'] + 10, xywh['y'] - 10), f"Element {index}", fill='Red', font=font)

        elements.append({
            'index': index,
            'type': 'text',
            'xywh': xywh,
            'confidence': 100, # FIXME: Fake confidence for now...
        })

        # The following code uploads the element to Minio. Disabled for now...
        # bytestream = io.BytesIO()
        # component_image.save(bytestream, format='JPEG', subsampling=0, quality=95)
        # bytestream.seek(0)

        # log.info('Uploading element to MinIO... (width: {}, height: {})'.format(xywh['w'], xywh['h']))
        # minio.put_object(outputBucketName, f'{outputBucketPath}/{index}.{outputFileFormat}', data=bytestream, length=bytestream.getbuffer().nbytes, content_type=outputContentType)
        # log.info('Uploaded element to MinIO') 
        # bytestream.close()
        component_image.close()

    bytestream = io.BytesIO()
    image.save(bytestream, format='JPEG', subsampling=0, quality=100)
    bytestream.seek(0)
    log.info('Uploading outline overview to MinIO...')
    minio.put_object(outputBucketName, f'{outputBucketPath}/elements.{outputFileFormat}', data=bytestream, length=bytestream.getbuffer().nbytes, content_type=outputContentType)
    bytestream.close()
    image.close()
    
    # Re-use the original bucket- and filename because each element is a bbox representation within the original image
    bucketName = job.get("bucketName")
    filename = job.get("filename")

    log.info("Job done")
    publish("elementResult", {'elementCount': elementCount, 'bucketName': bucketName, 'bucketPath': filename, 'fileExtension': 'jpg', 'elements': elements}, success=True, confidence=100)

    
if __name__ == '__main__':
    try:
        worker = kimiworker.Worker(jobHandler, "worker-convert-page-to-elements", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print("\n")
        print('Got interruption signal. Exiting...\n')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)